const readline = require('readline');
var fs = require('fs');

argDictionary = false;
argText = false;
fileDictionary = false;
fileText = false

function processArguments(){
  arguments = process.argv.slice(2);
  error = false;
  arguments.forEach((arg, index) => {
    if (arg == "-t") {
      argText = arguments[index+1];
      if (fileText) {
        error = "Error: only either -t or -T can be defined at the same time";
      }
    }
    if (arg == "-T") {
      fileText = arguments[index+1];
      if (argText) {
        error = "Error: only either -t or -T can be defined at the same time";
      }
    }
    if (arg == "-d") {
      argDictionary = arguments[index+1];
      if (fileText){
        error = "Error: only either -f or -F can be defined at the same time";
      }
    }
    if (arg == "-D") {
      fileDictionary = arguments[index+1];
      if (argText){
        error = "Error: only either -f or -F can be defined at the same time";
      }
    }
  });
  return error;
}

function interactiveInput(message){
  return new Promise((resolve, reject) => {
    const rl = readline.createInterface({
      input: process.stdin,
      output: process.stdout
    });
    rl.question(message, (str) => {
      input = str.trimLeft().trimRight();
      rl.close();
      resolve(input);
    })
  });
}

function openFile(path) {
  return new Promise((resolve, reject) => {
    fs.readFile(path, 'utf8', (err, contents) => {
      if (err) {
        console.log(err)
        reject(err);
      } else {
        resolve(contents.trimRight().trimLeft());
      }
    })
  });
}


function isEven(n) {
   return n % 2 == 0;
}

function fillDictionary(dicstr){
  dictionary = {}
  dictionary.entries = dicstr.toLowerCase().split("");
  if (isEven(dictionary.entries.length)) {
    dictionary.step = dictionary.entries.length/2;
    return dictionary;
  } else {
    console.log("The number of symbols in dictionary must be even");
    return false;
  }
}

function rotate(text, dictionary){
  rotatedtext = "";
  textarray = text.split("");
  textarray.forEach((element) => {
    upperCase = element.toUpperCase() === element;
    element = element.toLowerCase();
    elementIndex = dictionary.entries.indexOf(element);
    if (elementIndex>=0){
      if (elementIndex+dictionary.step>dictionary.entries.length-1) {
        newCharacter = dictionary.entries[elementIndex-dictionary.entries.length+dictionary.step];
      } else {
        newCharacter = dictionary.entries[elementIndex+dictionary.step];
      }
    } else {
      newCharacter = element;
    }
    if (upperCase) { newCharacter = newCharacter.toUpperCase()}
    rotatedtext = rotatedtext + newCharacter;
  });
  return rotatedtext;
}

// --------------------INTERACTIVE MODE FLOW FUNCTIONS--------------------------

function processDictionary(){
  if (argDictionary) {
    processText(argDictionary);
  } else if (fileDictionary) {
    openFile(fileDictionary).then((contents) => {
      processText(contents);
    });
  } else {
    interactiveInput("Enter dictionary string: ").then((str) => {
      processText(str);
    })
  }
}

function processText(dicString) {
  dictionary = fillDictionary(dicString);
  if (!dictionary) { return 1 };
  if (argText) {
    processRotate(argText, dictionary);
  } else if (fileText) {
    openFile(fileText).then((contents) => {
      processRotate(contents, dictionary);
    });
  } else {
    interactiveInput("Enter text to en/decrypt: ").then((str) => {
      processRotate(str, dictionary);
    })
  }
}

function processRotate(text, dictionary){
  console.log(rotate(text, dictionary));
  return 0;
}
// --------------------------- EXPORTS FUNCTION --------------------------------

exports.encode = function (text, dictionary) {
  dict = fillDictionary("dictionary");
  if (!dict) {
    return false;
  } else {
    return rotate(text, dict);
  }
}

// -------------------------- INTERACTIVE MODE ---------------------------------

argError = processArguments();
if (!argError){
  processDictionary();
} else {
  console.log(argError);
  return 1;
}
